<?php
/**
 * Plugin Name: HIOICE Legal Footer
 * Description: Adds Legal Details Widget to WordPress Theme. 
 * Author: Dan Nagle
 * Text Domain: hioice-legal
 * Domain Path: /lang/
 * Version: 1.0.0
 * License: GPLv2
 * Bitbucket Plugin URI: https://bitbucket.org/dnagle/hioice-legal-footer
 *
 * @package WordPress
 * @author Dan Nagle
 */

if ( !defined( 'HIOICE_LF_VERSION' ) ) {
	define( 'HIOICE_LF_VERSION', '1.0.0' ); // Plugin version
}
if ( !defined( 'HIOICE_LF_DIR' ) ) {
	define( 'HIOICE_LF_DIR', dirname( __FILE__ ) ); // Plugin directory
}
if ( !defined( 'HIOICE_LF_URL' ) ) {
	define( 'HIOICE_LF_URL', plugin_dir_url( __FILE__ ) ); // Plugin url
}
if ( !defined( 'HIOICE_LF_DOMAIN' ) ) {
	define( 'HIOICE_LF_DOMAIN', 'hioice-legal' ); // Text Domain
}
if ( !defined( 'HIOICE_LF_SECTION' ) ) {
	define( 'HIOICE_LF_SECTION', 'hioice_legal_details' ); // Text Domain
}

require_once HIOICE_LF_DIR . '/includes/hioice-legal-customizer.php';

require_once HIOICE_LF_DIR . '/includes/hioice-legal-shortcodes.php';
