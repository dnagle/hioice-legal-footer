<?php

add_shortcode( 'hioice_legal_footer', 'hioice_sc_legal_footer' );

function hioice_sc_legal_footer( $atts ) {

	$defaults = array(
		'class' => '',
		'copywriter' => (! empty(get_theme_mod('hioice_contact_company_name')) ? get_theme_mod('hioice_contact_company_name') : get_bloginfo('name')),
		'privacy' => true,
		'privacy_link' => (! empty(get_theme_mod('hioice_privacy_page_id')) ? get_permalink(get_theme_mod('hioice_privacy_page_id')) : ''),
		'privacy_title' => (! empty(get_theme_mod('hioice_privacy_link_title')) ? get_theme_mod('hioice_privacy_link_title') : ''),
		'privacy_text' => (! empty(get_theme_mod('hioice_privacy_link_text')) ? get_theme_mod('hioice_privacy_link_text') : ''),
		'cookies' => true,
		'cookies_link' => (! empty(get_theme_mod('hioice_cookies_page_id')) ? get_permalink(get_theme_mod('hioice_cookies_page_id')) : ''),
		'cookies_title' => (! empty(get_theme_mod('hioice_cookies_link_title')) ? get_theme_mod('hioice_cookies_link_title') : ''),
		'cookies_text' => (! empty(get_theme_mod('hioice_cookies_link_text')) ? get_theme_mod('hioice_cookies_link_text') : ''),
		'company' => true,
		'company_label' => __( 'Company Registration', HIOICE_LF_DOMAIN ),
		'company_reg' => (! empty(get_theme_mod('hioice_contact_company_reg')) ? get_theme_mod('hioice_contact_company_reg') : ''),
		'tax' => false,
		'tax_label' => __( 'Tax Registration', HIOICE_LF_DOMAIN ),
		'tax_reg' => (! empty(get_theme_mod('hioice_contact_tax_reg')) ? get_theme_mod('hioice_contact_tax_reg') : '')
	);

	extract( shortcode_atts($defaults, $atts) );

	$content = '<div class="hioice-legal-info';
	if ( ! empty($class) ) {
		$content .= ' ' . $class;
	}
	$content .= '"><p>Copyright &copy; ' . date('Y');
	if ( ! empty($copywriter) ) {
		$content .= ' &middot; ' . $copywriter;
	}
	if ( $privacy && ! empty($privacy_link) ) {
		$content .= ' &middot; <a href="'. $privacy_link .'"';
		if ( ! empty($privacy_title) ) {
			$content .= ' title="' . $privacy_title . '"';
		}
		$content .= '>' . $privacy_text . '</a>';
	}
	if ( $cookies && ! empty($cookies_link) ) {
		$content .= ' &middot; <a href="'. $cookies_link .'"';
		if ( ! empty($cookies_title) ) {
			$content .= ' title="' . $cookies_title . '"';
		}
		$content .= '>' . $cookies_text . '</a>';
	}
	if ( $company && ! empty($company_reg) ) {
		$content .= ' &middot; ' . $company_label . ': ' . $company_reg;
	}
	if ( $tax && ! empty($tax_reg) ) {
		$content .= ' &middot; ' . $tax_label . ': ' . $tax_reg;
	}
	$content .= '</p></div>';

	$output = do_shortcode( $content, true );

	echo $output;
}
