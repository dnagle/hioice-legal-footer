<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Register settings and controls with the Customizer.
 *
 * @since 1.0.0
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
add_action( 'customize_register', 'hioice_legal_customizer_register' );
function hioice_legal_customizer_register( $wp_customize ) {

	$wp_customize->add_section( HIOICE_LF_SECTION, array(
		'title' => __( 'Legal Details', HIOICE_LF_DOMAIN ),
		'description' => __('Add legal requirement details to your website footer', HIOICE_LF_DOMAIN ),
		'capability' => 'edit_theme_options'
	));

	/* Cookies */
	$wp_customize->add_setting( 'hioice_cookies_page_id', array(
	  'capability' => 'edit_theme_options',
	  'sanitize_callback' => 'hioice_sanitize_dropdown_pages',
	) );

	$wp_customize->add_control( 'hioice_cookies_page_id', array(
	  'type' => 'dropdown-pages',
	  'section' => HIOICE_LF_SECTION, // Add a default or your own section
	  'label' => __( 'Cookies Page', HIOICE_LF_DOMAIN ),
	  'description' => __( 'Cookies information page.', HIOICE_LF_DOMAIN ),
	) );

	$wp_customize->add_setting( 'hioice_cookies_link_title', array(
		'default'           => __( 'Cookie Policy' , HIOICE_LF_DOMAIN )
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize, 'hioice_cookies_link_title', array(
		'label'      => __( 'Cookies Link Title', HIOICE_LF_DOMAIN ),
		'description' => __( 'Accessibility text.', HIOICE_LF_DOMAIN ),
		'section'    => HIOICE_LF_SECTION,
		'settings'   => 'hioice_cookies_link_title',
		'type'       => 'text',
	) ) );

	$wp_customize->add_setting( 'hioice_cookies_link_text', array(
		'default'           => __( 'Cookies' , HIOICE_LF_DOMAIN )
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize, 'hioice_cookies_link_text', array(
		'label'      => __( 'Cookies Link Text', HIOICE_LF_DOMAIN ),
		'description' => __( 'Anchor text.', HIOICE_LF_DOMAIN ),
		'section'    => HIOICE_LF_SECTION,
		'settings'   => 'hioice_cookies_link_text',
		'type'       => 'text',
	) ) );

	/* Privacy Statement */
	$wp_customize->add_setting( 'hioice_privacy_page_id', array(
	  'capability' => 'edit_theme_options',
	  'sanitize_callback' => 'hioice_sanitize_dropdown_pages',
	) );

	$wp_customize->add_control( 'hioice_privacy_page_id', array(
	  'type' => 'dropdown-pages',
	  'section' => HIOICE_LF_SECTION, // Add a default or your own section
	  'label' => __( 'Privacy Statement', HIOICE_LF_DOMAIN ),
	  'description' => __( 'Privacy Statement information page.', HIOICE_LF_DOMAIN ),
	) );

	$wp_customize->add_setting( 'hioice_privacy_link_title', array(
		'default'           => __( 'Privacy Statement' , HIOICE_LF_DOMAIN )
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize, 'hioice_privacy_link_title', array(
		'label'      => __( 'Privacy Statement Link Title', HIOICE_LF_DOMAIN ),
		'description' => __( 'Accessibility text.', HIOICE_LF_DOMAIN ),
		'section'    => HIOICE_LF_SECTION,
		'settings'   => 'hioice_privacy_link_title',
		'type'       => 'text',
	) ) );

	$wp_customize->add_setting( 'hioice_privacy_link_text', array(
		'default'           => __( 'Privacy' , HIOICE_LF_DOMAIN )
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize, 'hioice_privacy_link_text', array(
		'label'      => __( 'Privacy Statement Link Text', HIOICE_LF_DOMAIN ),
		'description' => __( 'Anchor text.', HIOICE_LF_DOMAIN ),
		'section'    => HIOICE_LF_SECTION,
		'settings'   => 'hioice_privacy_link_text',
		'type'       => 'text',
	) ) );

}

function hioice_sanitize_dropdown_pages( $page_id, $setting ) {
	// Ensure $input is an absolute integer.
	$page_id = absint( $page_id );
	// If $page_id is an ID of a published page, return it; otherwise, return the default.
	return ( 'publish' == get_post_status( $page_id ) ? $page_id : $setting->default );
}
